export interface TimeSpec {
    day?: string // courses
    date_start?: string // courses
    date_end?: string // courses
    time?: string // workshops & courses
    date?: string // workshops
}