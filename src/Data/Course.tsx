import {Teacher} from "./Teacher";
import {TimeSpec} from "./TimeSpec";

export interface Course {
    title: string
    teacher?: Teacher
    teacher_slug?: Teacher
    price: string
    flavor_image?: string
    id: string
    description: string
    modified: boolean
    days?: TimeSpec[]
}