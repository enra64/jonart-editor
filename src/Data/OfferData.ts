import {Course} from "./Course";

export default interface OfferData {
    courses: Course[],
    kids: Course[],
    workshops: Course[]
}