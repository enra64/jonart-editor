export default interface Page {
    content: string
    category: string
    title: string
    zipFileName: string
    id: string
    modified: boolean
}