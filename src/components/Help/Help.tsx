import React from "react";
import Alert from "react-bootstrap/Alert";
import {LinkContainer} from 'react-router-bootstrap'
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import "./Help.css"

interface HelpState {
    showAlert: boolean
}

interface HelpProps {
    errors?: string
}

export default class Help extends React.Component<HelpProps, HelpState> {
    constructor(props: HelpProps) {
        super(props);

        const showAlert = localStorage.getItem("showNoEditAlert") !== "false"  // != false to default to true
        this.state = {
            showAlert
        }

        this.noUpdateAlert = this.noUpdateAlert.bind(this);
    }

    private noUpdateAlert() {
        if (this.state.showAlert) {
            return <Alert variant="primary" onClose={() => {
                localStorage.setItem("showNoEditAlert", "false")
                this.setState({showAlert: false})
            }} dismissible>
                <p className={"mt-3"}>
                    Dieser Editor kann jonart-malschule.de nicht direkt verändern. Stattdessen wird eine
                    Export-Datei erstellt, die alle Änderungen enthält. Die Export-Datei kannst du an deinen
                    Lieblingsadministrator schicken, der das Update dann ganz einfach einspielen kann.
                </p>
            </Alert>;
        }
        return <div/>;
    }

    private static programmEditor() {
        return <div className={"mb-4 mb-md-0"}>
            <h4>Programmeditor</h4>
            <p>
                Mit dem Programmeditor kannst du die Programmboxen aktualisieren, inklusive Lehrer, Preis,
                Beschreibung, und Titel.
            </p>
            <LinkContainer to="/programmeditor">
                <Button>Zum Programmeditor</Button>
            </LinkContainer>
        </div>
    }

    private static textEditor() {
        return <div className={"mb-4 mb-md-0"}>
            <h4>Texteditor</h4>
            <p>
                Mit dem Texteditor kannst du alle Texte ändern, die nicht in den Programmboxen stehen.
            </p>

            <LinkContainer to="/texteditor">
                <Button>Zum Texteditor</Button>
            </LinkContainer>
        </div>
    }

    private static export() {
        return <div className={"mb-4 mb-md-0"}>
            <h4>Exporteur</h4>
            <p>
                Mit dem Exporteur kannst du die Export-Datei erstellen, die du einfach per Mail
                versenden kannst.
            </p>
            <LinkContainer to="/exportieren">
                <Button>Zum Exporteur</Button>
            </LinkContainer>
        </div>
    }

    private static renderErrors(errors?: string): any {
        if (!errors) {
            return <div/>
        }

        return <Alert variant="danger">
            {errors}
        </Alert>
    }

    private static renderHeroImage(): any {
        return <div className={"hero jumbotron jumbotron-fluid"}>
            <div className={"container py-lg-5"}>
                <h1 className="display-4">Herzlich Will&shy;kommen beim <a
                    href={"https://jonart-malschule.de"}>JonArt</a>-Editor</h1>
                <p className={"lead pb-lg-0 pb-2"}>
                    Du suchst eigentlich nach unserer Malschule JonArt? Dann besuch fix <a
                    href={"https://jonart-malschule.de"}>jonart-malschule.de</a>
                </p>
            </div>
        </div>
    }

    render() {
        return <div>
            {Help.renderHeroImage()}
            <Container>
                <div className={"pt-4"}>
                    <p>
                        Ich hoffe du kommst gut mit dem Editor klar. Wenn du etwas nicht verstehst oder du einen
                        Verbesserungsvorschlag hast, schreib mich einfach an.
                        Die einzelnen Funktionen kannst du mit den Knöpfen unten oder mit der Menüleiste oben anwählen.
                    </p>
                    <p>
                        Diese Seite vergisst deine Eingaben, wenn du sie schließt oder neu lädst! Also lieber
                        öfter mal exportieren. Mehrere dieser Dateien zu laden ist kein Problem für mich.
                    </p>
                    {this.noUpdateAlert()}
                    {Help.renderErrors(this.props.errors)}
                    <Row className={"my-4"}>
                        <Col md>{Help.programmEditor()}</Col>
                        <Col md>{Help.textEditor()}</Col>
                        <Col md>{Help.export()}</Col>
                    </Row>
                </div>
            </Container>
        </div>
    }
}