import React from 'react';
import './OfferList.css';
import {Course} from "../../../Data/Course";
import Row from "react-bootstrap/Row";
import ListGroup from "react-bootstrap/ListGroup";
import OfferEditor from "../OfferEditor/OfferEditor";
import Col from "react-bootstrap/Col";
import OfferData from "../../../Data/OfferData";
import {FiEdit3} from "react-icons/fi";

interface OfferListProps {
    courses: Course[]
    workshops: Course[]
    kids: Course[]
    onChange: (dataExport: OfferData) => void
}

type OfferMap = { [uuid: string]: Course };

interface OfferListState {
    courses: OfferMap
    workshops: OfferMap
    kids: OfferMap
    openModal?: Course
    openModalId?: string
}

export default class OfferList extends React.Component<OfferListProps, OfferListState> {
    constructor(offers: OfferListProps) {
        super(offers);
        this.state = {
            courses: OfferList.toKeyMap(offers.courses),
            workshops: OfferList.toKeyMap(offers.workshops),
            kids: OfferList.toKeyMap(offers.kids)
        };

        this.onCourseClick = this.onCourseClick.bind(this);
        this.renderItem = this.renderItem.bind(this);
        this.renderList = this.renderList.bind(this);
        this.forwardOnChange = this.forwardOnChange.bind(this);
        this.updateCourse = this.updateCourse.bind(this);
    }

    private static toKeyMap(offers: Course[]): OfferMap {
        const offerMap: OfferMap = {};
        offers.forEach((offer) => offerMap[offer.id] = offer)
        return offerMap;
    }

    private onCourseClick(id: string, course: Course) {
        this.setState({openModal: course, openModalId: id})
    }

    private renderItem(id: string, course: Course): any {
        return <ListGroup.Item action key={id} onClick={() => this.onCourseClick(id, course)}>
            <div className={"courseListItem"}>
                <div>
                    {course.title}
                </div>
                <div>
                    {course.modified ? <FiEdit3 size={"1.5em"}/> : <div/>}
                </div>
            </div>
        </ListGroup.Item>;
    }

    private renderList(offerMap: OfferMap): any {
        const items = [];
        for (const uuid in offerMap) {
            items.push(this.renderItem(uuid, offerMap[uuid]))
        }
        return <ListGroup>{items}</ListGroup>
    }

    private forwardOnChange(kids: OfferMap, courses: OfferMap, workshops: OfferMap) {
        this.props.onChange({
            kids: Object.keys(kids).map(key => kids[key]),
            courses: Object.keys(courses).map(key => courses[key]),
            workshops: Object.keys(workshops).map(key => workshops[key])
        })
    }

    private updateCourse(id: string, course: Course) {
        // @ts-ignore
        this.setState((state) => {
            const {courses, workshops, kids} = state;
            if(id in workshops) {
                const workshops = {...this.state.workshops, [id]: course};
                this.forwardOnChange(state.kids, state.courses, workshops)
                return {workshops, openModal: undefined, openModalId: undefined};
            } else if (id in courses) {
                const courses = {...this.state.courses, [id]: course}
                this.forwardOnChange(state.kids, courses, state.workshops)
                return {courses, openModal: undefined, openModalId: undefined};
            } else if (id in kids) {
                const kids = {...this.state.kids, [id]: course};
                this.forwardOnChange(kids, state.courses, state.workshops)
                return {kids, openModal: undefined, openModalId: undefined};
            } else {
                throw new Error("trying to update course, but couldn't find ID anywhere")
            }
        })
    }

    private renderModal(): any {
        if (this.state.openModal && this.state.openModalId) {
            return <OfferEditor
                course={this.state.openModal}
                courseId={this.state.openModalId}
                onClose={() => this.setState({openModal: undefined, openModalId: undefined})}
                onSave={this.updateCourse}/>
        } else if (this.state.openModal || this.state.openModalId) {
            throw new Error("bad openModal spec: id or course unset!")
        }
        return <div/>
    }

    render() {
        const {courses, workshops, kids} = this.state;
        return <div>
            {this.renderModal()}
            <Row>
                <Col>
                    <h1>Programmeditor</h1>
                </Col>
            </Row>
            <Row>
                <Col lg>
                    <div className={"d-flex-column"}>
                        <h3 className={"mt-4 mb-2"}>Kurse</h3>
                        {this.renderList(courses)}
                    </div>
                </Col>
                <Col lg>
                    <div className={"d-flex-column"}>
                        <h3 className={"mt-4 mb-2"}>Workshops</h3>
                        {this.renderList(workshops)}
                    </div>
                </Col>
                <Col lg>
                    <div className={"d-flex-column"}>
                        <h3 className={"mt-4 mb-2"}>Kinder</h3>
                        {this.renderList(kids)}
                    </div>
                </Col>
            </Row>
        </div>
    }
}
