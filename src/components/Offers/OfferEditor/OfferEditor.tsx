import React from 'react';
import './OfferEditor.css';
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {v4 as uuidv4} from 'uuid';

import {Course} from "../../../Data/Course";
import {TimeSpec} from "../../../Data/TimeSpec";
import {Teacher} from "../../../Data/Teacher";

interface OfferEditorProps {
    course: Course
    courseId: string
    onSave: (id: string, updatedCourse: Course) => void
    onClose: () => void
}

interface OfferEditorState {
    title: string
    teacher?: Teacher
    teacher_slug?: Teacher
    price: string
    id: string
    modified: boolean
    flavor_image?: string
    description: string
    days: { [uuid: string]: TimeSpec }
}

export default class OfferEditor extends React.Component<OfferEditorProps, OfferEditorState> {
    constructor(offerEditorProps: OfferEditorProps) {
        super(offerEditorProps);
        const initialState = {
            title: offerEditorProps.course.title,
            teacher: offerEditorProps.course.teacher,
            teacher_slug: offerEditorProps.course.teacher_slug,
            price: offerEditorProps.course.price,
            flavor_image: offerEditorProps.course.flavor_image,
            description: offerEditorProps.course.description,
            id: offerEditorProps.course.id,
            days: {} as { [uuid: string]: TimeSpec },
            modified: false
        }

        if (offerEditorProps.course.days) {
            for (const day of offerEditorProps.course.days) {
                initialState.days[uuidv4()] = day;
            }
        }

        this.state = initialState;
        this.renderDay = this.renderDay.bind(this);
        this.renderDays = this.renderDays.bind(this);
        this.addCourseDay = this.addCourseDay.bind(this);
        this.addWorkshopDay = this.addWorkshopDay.bind(this);
        this.removeDay = this.removeDay.bind(this);
        this.handleSaving = this.handleSaving.bind(this);
        this.updateDay = this.updateDay.bind(this);
    }

    private removeDay(id: string) {
        this.setState((state) => {
            const days = {...state.days};
            delete days[id];
            return {days, modified: true};
        })
    }

    private addCourseDay() {
        const days = {...this.state.days};
        days[uuidv4()] = {
            day: "Montags", // courses
            date_start: "01. 12. 20", // courses
            date_end: "01. 01. 20", // courses
            time: "12 Uhr" // courses
        }
        this.setState({days, modified: true})
    }

    private addWorkshopDay() {
        const days = {...this.state.days};
        days[uuidv4()] = {
            time: "12 Uhr", // workshops
            date: "24. 12. 2020" // workshops
        }
        this.setState({days, modified: true})
    }

    private updateDay(id: string, key: string, value: string) {
        this.setState((state) => {
            if (state.days && id in state.days) {
                const days = {...state.days};
                // @ts-ignore
                days[id][key] = value;
                return {days, modified: true};
            } else {
                throw new Error(`${id} not found in ${state.days} for update`)
            }
        })
    }

    private renderDay(day: TimeSpec, id: string): any {
        let form;
        if (day.date_start) {
            form = <div>
                <div className={"m-2 offerDayTitle"}>
                    <h5 className={"m-0 mt-1"}>
                        {day.day}, {day.time}
                    </h5>
                    <button type="button" className="close" aria-label="Close" onClick={() => this.removeDay(id)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <hr className={"mb-0 mt-2 bg-primary"}/>
                <div className={"mx-2 mb-2"}>
                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Wochentag</Form.Label>
                                <Form.Control type={"text"} value={day.day}
                                              onChange={(e) => this.updateDay(id, "day", e.target.value)}/>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Uhrzeit</Form.Label>
                                <Form.Control type={"text"} value={day.time}
                                              onChange={(e) => this.updateDay(id, "time", e.target.value)}/>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Startdatum</Form.Label>
                                <Form.Control type={"text"} value={day.date_start}
                                              onChange={(e) => this.updateDay(id, "date_start", e.target.value)}/>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Enddatum</Form.Label>
                                <Form.Control type={"text"} value={day.date_end}
                                              onChange={(e) => this.updateDay(id, "date_end", e.target.value)}/>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                </div>
            </div>
        } else {
            form = <div>
                <div className={"m-2 offerDayTitle"}>
                    <h5 className={"m-0 mt-1"}>
                        {day.date}, {day.time}
                    </h5>
                    <button type="button" className="close" aria-label="Close" onClick={() => this.removeDay(id)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <hr className={"mb-0 mt-2 bg-primary"}/>
                <div className={"mx-2 mb-2"}>
                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Datum</Form.Label>
                                <Form.Control type={"text"} value={day.date}
                                              onChange={(e) => this.updateDay(id, "date", e.target.value)}/>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Zeit</Form.Label>
                                <Form.Control type={"text"} value={day.time}
                                              onChange={(e) => this.updateDay(id, "time", e.target.value)}/>
                            </Form.Group>
                        </Col>
                    </Form.Row>
                </div>
            </div>;
        }

        return <div className={"my-2 rounded border border-primary"}>
            {form}
        </div>
    }

    private static renderTeacherOptions(): any {
        const options = [];
        for (let teacherSlug in Teacher) {
            const teacherSlugKey = teacherSlug as keyof typeof Teacher;
            options.push(<option key={teacherSlugKey} value={teacherSlugKey}>{Teacher[teacherSlugKey]}</option>)
        }
        return options;
    }

    private renderDays() {
        const days = [];

        for (const id in this.state.days) {
            days.push(this.renderDay(this.state.days[id], id))
        }

        return <div className={"mt-3"}>
            <h4>Angebotstage</h4>
            {days}
        </div>
    }

    private renderForm() {
        return <Form>
            <Form.Group>
                <Form.Label>Kursname</Form.Label>
                <Form.Control type="text" placeholder={"Kursname"} value={this.state.title}
                              onChange={(e) => this.setState({title: e.target.value, modified: true})}/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Dozentin</Form.Label>
                <Form.Control value={this.state.teacher_slug} as={"select"}
                              onChange={(e) => this.setState({
                                  teacher: Teacher[e.target.value as keyof typeof Teacher],
                                  teacher_slug: e.target.value as Teacher,
                                  modified: true
                              })}>
                    {OfferEditor.renderTeacherOptions()}
                </Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Preis</Form.Label>
                <Form.Control type={"text"} value={this.state.price}
                              onChange={(e) => this.setState({price: e.target.value, modified: true})}
                              placeholder={"Kurspreis"}/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Beschreibung</Form.Label>
                <Form.Control value={this.state.description} as="textarea"
                              onChange={(e) => this.setState({description: e.target.value, modified: true})} rows={5}/>
            </Form.Group>
            <div>
                {this.renderDays()}
            </div>
            <div>
                <Button variant={"outline-success"} className={"mr-4"} onClick={this.addWorkshopDay}>Neuer Einmal-Termin</Button>
                <Button variant={"outline-success"} className={"mr-4"} onClick={this.addCourseDay}>Neuer wöchentlicher Termin</Button>
            </div>
        </Form>;
    }

    private handleSaving() {
        const dayList: TimeSpec[] = [];
        for (const id in this.state.days) {
            dayList.push(this.state.days[id])
        }
        this.props.onSave(this.props.courseId, {...this.state, days: dayList})
    }

    render() {
        return <Modal show={true}
                      onHide={this.props.onClose}
                      size={"lg"}
                      backdrop="static">

            <Modal.Header closeButton>
                <Modal.Title>{this.state.title}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {this.renderForm()}
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={this.props.onClose}>Abbrechen</Button>
                <Button variant="primary" onClick={this.handleSaving}>Fertig</Button>
            </Modal.Footer>
        </Modal>
    }
}
