import React from 'react';
import './OfferEditorApp.css';
import OfferList from "../OfferList/OfferList";
import OfferData from "../../../Data/OfferData";
import LoadingSpinner from "../../LoadingSpinner/LoadingSpinner";


interface OfferEditorAppProps {
    data?: OfferData
    onChange: (dataExport: OfferData) => void
}

export default class OfferEditorApp extends React.Component<OfferEditorAppProps> {
    render() {
        if (this.props.data) {
            const {courses, kids, workshops} = this.props.data;
            return <OfferList onChange={this.props.onChange} courses={courses} kids={kids} workshops={workshops}/>
        } else {
            return <LoadingSpinner/>
        }
    }
}
