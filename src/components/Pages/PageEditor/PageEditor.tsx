import Page from "../../../Data/Page";
import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import {v4 as uuidv4} from 'uuid';
import Badge from "react-bootstrap/Badge";

interface PageEditorProps {
    page?: Page
    pageId?: string
    onSave: (id: string, newVersion: Page) => void
    onClose: () => void
}

interface PageEditorState {
    page: Page
    pageId: string
}

export default class PageEditor extends React.Component<PageEditorProps, PageEditorState> {
    constructor(props: PageEditorProps) {
        super(props);

        if (props.page && props.pageId) {
            this.state = {
                page: props.page,
                pageId: props.pageId
            }
        } else if (!props.page && !props.pageId) {
            const id = uuidv4()
            this.state = {
                page: {
                    content: "",
                    title: "Neue Seite (dubios)",
                    id,
                    zipFileName: "hmmm",
                    category: "hmmmmm",
                    modified: true
                },
                pageId: id
            }
        }

        this.renderCheck = this.renderCheck.bind(this);
        this.renderForm = this.renderForm.bind(this);
        this.handleSaving = this.handleSaving.bind(this);
    }

    private static checkParenthesesBalanced(expr: string) {
        // https://stackoverflow.com/a/52970433
        let holder = []
        let openBrackets = ['(', '{', '[', "<"]
        let closedBrackets = [')', '}', ']', ">"]
        for (let letter of expr) { // loop trought all letters of expr
            if (openBrackets.includes(letter)) { // if its oppening bracket
                holder.push(letter)
            } else if (closedBrackets.includes(letter)) { // if its closing
                let openPair = openBrackets[closedBrackets.indexOf(letter)] // find his pair
                if (holder[holder.length - 1] === openPair) { // check if that pair is last element in array
                    holder.splice(-1, 1) //if so, remove it
                } else { // if its not
                    holder.push(letter)
                    break // exit loop
                }
            }
        }
        return holder.length === 0 // return true if length is 0, otherwise false
    }

    private renderForm() {
        return <Form>
            <Form.Group>
                <Form.Control value={this.state.page.content} as="textarea"
                              onChange={(e) => {
                                  const newContent = e.target.value;
                                  this.setState((state) => {
                                      const page = {...state.page}
                                      page.content = newContent;
                                      page.modified = true;
                                      return {page}
                                  });
                              }} rows={15}/>
            </Form.Group>
        </Form>;
    }

    private renderCheck() {
        const content = this.state.page.content;
        const evenQuotationMarkCounts = content.split("'").length % 2 && content.split('"').length % 2
        const quotationMarksBadge = evenQuotationMarkCounts ?
            <Badge variant="success">OK</Badge> :
            <Badge variant="warning">Bitte überprüfe ob alle Anführungszeichen in Paaren stehen</Badge>;

        const balancedParentheses = PageEditor.checkParenthesesBalanced(content)
        const balancedParenthesesBadge = balancedParentheses ?
            <Badge variant="success">OK</Badge> :
            <Badge variant="warning">Bitte überprüfe ob alle Klammern in Paaren stehen</Badge>;

        return <div>
            <div>
                Klammern in Paaren: {balancedParenthesesBadge}
            </div>
            <div>
                Anführungszeichen in Paaren: {quotationMarksBadge}
            </div>
        </div>
    }

    private handleSaving() {

        this.props.onSave(this.state.pageId, this.state.page)
    }

    render() {
        return <Modal show={true}
                      onHide={this.props.onClose}
                      size={"lg"}
                      backdrop="static">

            <Modal.Header closeButton>
                <Modal.Title>{this.state.page.title}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {this.renderForm()}
                {this.renderCheck()}
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={this.props.onClose}>Abbrechen</Button>
                <Button variant="primary" onClick={this.handleSaving}>Fertig</Button>
            </Modal.Footer>
        </Modal>
    }
}