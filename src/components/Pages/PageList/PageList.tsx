import React from 'react';
import Row from "react-bootstrap/Row";
import ListGroup from "react-bootstrap/ListGroup";
import {Pages} from "../../../Data/Pages";
import Page from "../../../Data/Page";
import PageEditor from "../PageEditor/PageEditor";
import Col from "react-bootstrap/Col";
import "./PageList.css";
import Form from "react-bootstrap/Form";
import PageListHelp from "./PageListHelp";
import {FiEdit3} from "react-icons/fi";

interface PageListProps {
    pages: Pages
    onChange: (pageExport: Pages) => void
}

interface PageListState {
    pages: Pages
    searchTerm?: string
    openPage?: Page
}

export default class PageList extends React.Component<PageListProps, PageListState> {
    constructor(offers: PageListProps) {
        super(offers);
        this.state = {
            pages: offers.pages,
        };

        this.onPageClick = this.onPageClick.bind(this);
        this.renderItem = this.renderItem.bind(this);
        this.renderPages = this.renderPages.bind(this);
        this.forwardOnChange = this.forwardOnChange.bind(this);
        this.updatePage = this.updatePage.bind(this);
        this.renderSearch = this.renderSearch.bind(this);
    }

    private onPageClick(page: Page) {
        this.setState({openPage: page})
    }

    private renderItem(page: Page): any {
        return <ListGroup.Item
            className={"py-2 px-3"}
            action key={page.id}
            onClick={() => this.onPageClick(page)}>
            <div className={"pageListItem"}>
                <div>
                    <h6>{page.title}</h6>
                    <p className={"text-muted"}><small>Ordner: {page.category}</small></p>
                </div>
                <div>
                    {page.modified ? <FiEdit3 size={"1.5em"}/> : <div/>}
                </div>
            </div>

        </ListGroup.Item>;
    }

    private forwardOnChange(pages: Pages) {
        this.props.onChange(pages)
    }

    private updatePage(id: string, page: Page) {
        this.setState((state) => {
            let modifyCount = 0;
            const pages = state.pages.map((p) => {
                if (p.id === id) {
                    modifyCount++
                    return page
                } else {
                    return p
                }
            })

            if (modifyCount === 0) {
                throw new Error("Could not find modified page!")
            }

            this.forwardOnChange(pages)
            return {
                pages,
                openPage: undefined,
                openPageId: undefined
            }
        })
    }

    private renderModal(): any {
        if (this.state.openPage) {
            return <PageEditor
                page={this.state.openPage}
                pageId={this.state.openPage.id}
                onClose={() => this.setState({openPage: undefined})}
                onSave={this.updatePage}/>
        }
        return <div/>
    }

    private renderSearch(): any {
        return <div>
            <Form>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Suchtext" onChange={(e) => {
                                this.setState({
                                    searchTerm: e.target.value,
                                });
                            }}/>
                            <Form.Text className="text-muted">
                                Kopiere einen Ausschnitt des Textes, den du ändern möchtest, hierher
                            </Form.Text>
                        </Form.Group>
                    </Col>
                </Row>
            </Form>
        </div>
    }

    private renderPages(): any {
        const {searchTerm, pages} = this.state;

        if (searchTerm == null) {
            return null;
        }

        const result = pages
            .filter((p) => searchTerm != null && p.content.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0)
            .map(this.renderItem)

        return <div className={"my-2"}>
            <h2>{result.length} {result.length === 1 ? "Suchergebnis" : "Suchergebnisse"}</h2>
            <div className={"my-1"}>
                <ListGroup>{result}</ListGroup>
            </div>
        </div>
    }


    render() {
        return <div>
            <PageListHelp/>
            {this.renderSearch()}
            {this.renderPages()}
            {this.renderModal()}
        </div>
    }
}
