import React from "react";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Jumbotron from "react-bootstrap/Jumbotron";

export default class PageListHelp extends React.Component<{}, { show: boolean }> {
    constructor(props: {}) {
        super(props);
        const showHelp = localStorage.getItem("showPageEditorHelp") !== "false"  // != false to default to true
        this.state = {
            show: showHelp
        }
    }

    private renderHelpButton() {
        return <div className={"mb-2"}>
            <Button
                onClick={() => this.setState((state) => {
                    const newShowState = !state.show
                    localStorage.setItem("showPageEditorHelp", newShowState ? "true" : "false")
                    return {show: newShowState}
                })}>Hilfe {this.state.show ? "ausblenden" : "einblenden"}</Button>
        </div>
    }

    private static renderHelp() {
        const exampleDontModify = "{{< /row >}}\n" +
            "\n" +
            "{{< row >}}\n" +
            "{{< render-workshops >}}\n" +
            "{{< /row >}}";
        const exampleModify = "---\n" +
            "title: 'Über unsere Malschule'\n" +
            "weight: 2\n" +
            "background: '/images/flavor/staffeleien.jpg'\n" +
            "button: 'Mehr über die Künstlerinnen'\n" +
            "buttonLink: 'ueber-uns'\n" +
            "---\n\n" +
            "{{< image-bootstrap \"/images/leiterinnen/susanne.jpg\" \"Susanne Herdick\" >}}\n" +
            "\n" +
            "|                                                  |\n" +
            "|--------------------------------------------------|\n" +
            "| Schornstr. 30                                    |\n" +
            "| 45128 Essen                                      |\n" +
            "| {{< phone \"015773834923\" \"0157/73834923\" >}}     |\n" +
            "|                                                  |\n" +
            "| susanne[at]herdick.info                          |\n" +
            "| [susanne-herdick.de](https://susanne-herdick.de) |\n" +
            "\n" +
            "---\n" +
            "Susanne Herdick ist für diese Website verantwortlich.";

        return <Row>
            <Col>
                <p>
                    Der Texteditor erlaubt es dir, alle Texte außerhalb der Programmboxen zu verändern.
                    Die Texte sind auf viele verschiedene Dateien aufgeteilt. Deshalb gibt es die Suchfunktion.
                    Damit kannst du nach dem Text suchen, den du verändern möchtest und so die schnell
                    richtige Datei finden.
                </p>
                <p>
                    Ein Beispiel: angenommen, du möchtest den COVID-19-Hinweis ändern. Dann kopierst du
                    z.B. <code>"Coronavirus ein. So bald wie möglich geht"</code> in das Suchfeld.
                    Da diese Formulierung einmalig auf der Website ist, findest du sofort die Datei!
                    Klicke auf die vorgeschlagene Datei (in unserem Beispiel <code>notifications/covid19.md</code>)
                    und ändere den Text.
                </p>
                <p>
                    In den Texten sind viele Sonderzeichen (e.g. <code>{"'{}()<>\"---"}</code>) enthalten. Am besten, du
                    entfernst die nicht, sonst muss ich sie
                    manuell wieder hinzufügen. Wenn du deutschen Text zwischen den Sonderzeichen siehst, kannst du den
                    allerdings verändern.
                </p>
                <h4>Beispiele:</h4>
                <p>
                    Hier solltest du nichts verändern, denn das sind alles Instruktionen:
                </p>
                <Jumbotron>
                    <pre><code>{exampleDontModify}</code></pre>
                </Jumbotron>
                <p>
                    Im folgenden Beispiel kannst du die deutschen Texte ruhig verändern, achte nur darauf, keine
                    Sonderzeichen davor oder danach zu entfernen. Genauer gesagt, kannst du alles ändern, was auf der
                    Webseite genau so steht. <code>susanne[at]herdick.info</code> z.B. sieht auf der Webseite genauso
                    aus. Das bedeutet, es wird vom Computer nicht interpretiert (übersetzt), und du kannst es einfach
                    ändern. Das heißt, du kannst im Beispiel unten folgendes ändern:
                </p>
                <ul>
                    <li><code>Über unsere Malschule</code></li>
                    <li><code>Mehr über die Künstlerinnen</code></li>
                    <li><code>Schornstr. 30</code></li>
                    <li><code>45128 Essen</code></li>
                    <li><code>015773834923</code></li>
                    <li><code>0157/73834923</code></li>
                    <li><code>susanne[at]herdick.info</code> ([at] steht so auch auf der Webseite)</li>
                    <li><code>Susanne Herdick ist für diese Website verantwortlich.</code></li>
                </ul>
                <Jumbotron>
                    <pre><code>{exampleModify}</code></pre>
                </Jumbotron>
                <p>
                    Die meisten Dateien enthalten aber gar nicht so viele Sonderzeichen.
                    Es ist <b>überhaupt nicht schlimm</b>, wenn dir mal eins verloren geht. Ich kann das in der Export-Datei
                    recht einfach reparieren.
                </p>
            </Col>
        </Row>
    }

    render() {
        return <div>
            <div className={"pageListHelpHeader"}>
                <h1>Texteditor</h1>
                <div className={"mt-1"}>
                    {this.renderHelpButton()}
                </div>
            </div>
            {this.state.show && PageListHelp.renderHelp()}
        </div>
    }

}