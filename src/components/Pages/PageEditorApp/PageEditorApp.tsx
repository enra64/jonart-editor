import React from 'react';
import './PageEditorApp.css';
import PageList from "../PageList/PageList"
import {Pages} from "../../../Data/Pages";
import LoadingSpinner from "../../LoadingSpinner/LoadingSpinner";

interface PageEditorAppProps {
    data?: Pages
    onChange: (pageUpdate: Pages) => void
}

export default class PageEditorApp extends React.Component<PageEditorAppProps> {
    render() {
        return this.props.data ?
            <PageList onChange={this.props.onChange} pages={this.props.data}/> :
            <LoadingSpinner/>
    }
}
