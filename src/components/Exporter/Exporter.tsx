import React from "react";
import Button from "react-bootstrap/Button";
import JSZip from "jszip";
import {Pages} from "../../Data/Pages";
import {Course} from "../../Data/Course";
import OfferData from "../../Data/OfferData";
import {FiDownload} from "react-icons/fi";
import ListGroup from "react-bootstrap/ListGroup";
import Page from "../../Data/Page";
import DiffModal from "./DiffModal";
import stringify from "json-stable-stringify";

interface ExporterProps {
    import: { data?: OfferData, pages?: Pages }
    export: { data?: OfferData, pages?: Pages }
}

interface ExporterState {
    viewDiff?: Page | Course
}

const months = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September',
    'Oktober', 'November', 'Dezember'];

export default class Exporter extends React.Component<ExporterProps, ExporterState> {
    constructor(props: ExporterProps) {
        super(props);

        this.state = {}

        this.onExport = this.onExport.bind(this);
        this.renderModificationHints = this.renderModificationHints.bind(this);
        this.renderModifiedProgramHint = this.renderModifiedProgramHint.bind(this);
        this.renderModifiedPagesHint = this.renderModifiedPagesHint.bind(this);
    }

    private static removeModificationFlag(courses: Course[], removeTeacherSlug: boolean = false) {
        return courses.map((c) => {
            if (removeTeacherSlug) {
                c = {...c, teacher_slug: undefined}
            }
            return {...c, modified: undefined, id: undefined}
        });
    }

    private static addData(zip: JSZip, data: { courses: Course[], workshops: Course[], kids: Course[] }): JSZip {
        const {courses, workshops, kids} = data;
        if (courses.some((c) => c.modified)) {
            zip.file(
                "data/courses.json",
                JSON.stringify(Exporter.removeModificationFlag(courses), null, 4)
            );
        }
        if (workshops.some((c) => c.modified)) {
            zip.file(
                "data/workshops.json",
                JSON.stringify(Exporter.removeModificationFlag(workshops), null, 4)
            );
        }
        if (kids.some((c) => c.modified)) {
            zip.file(
                "data/kinderkurse.json",
                JSON.stringify(Exporter.removeModificationFlag(kids), null, 4)
            );
        }

        return zip
    }

    private static addPages(zip: JSZip, data: Pages): JSZip {
        data
            .filter((p) => p.modified)
            .forEach((p) => zip.file(p.zipFileName, p.content))
        return zip
    }

    private static getFilename() {
        const now = new Date();
        return `JonArt-Datenexport von ${months[now.getMonth()]} ${now.getDate()} um ${now.getHours()} Uhr ${now.getMinutes()}.zip`
    }

    private onExport() {
        let zip = new JSZip();
        if (this.props.export.data) {
            zip = Exporter.addData(zip, this.props.export.data);
        }
        if (this.props.export.pages) {
            zip = Exporter.addPages(zip, this.props.export.pages);
        }
        zip.generateAsync({type: "blob"}).then(blob => {
            const element = document.createElement("a");
            element.href = URL.createObjectURL(blob);
            element.download = Exporter.getFilename();
            document.body.appendChild(element);
            element.click();
        }, (err) => console.error(err));
    }

    // noinspection JSMethodCanBeStatic
    private renderModifiedPagesHint(pageExport?: Pages): any {
        const modified = pageExport?.filter((p) => p.modified)

        if (modified) {
            const modifiedItems = modified.map((m) =>
                <ListGroup.Item
                    action
                    onClick={() => this.setState({viewDiff: m})}
                    key={m.title}>
                    {m.title}
                </ListGroup.Item>)

            return <div>
                <p>
                    {modified.length === 1 ? "Eine Änderung" : (modified.length + " Änderungen")} an Texten
                </p>
                <ListGroup>
                    {modifiedItems}
                </ListGroup>
            </div>
        } else {
            return <p>Keine Änderungen an Texten</p>
        }
    }

    private renderModifiedProgramHint(dataExport?: OfferData): any {
        if (!dataExport) {
            return "Keine Änderungen am Programm"
        }

        const {courses, kids, workshops} = dataExport;
        const modified: Course[] = [];
        courses.filter((c) => c.modified).forEach((c) => modified.push(c))
        kids.filter((c) => c.modified).forEach((c) => modified.push(c))
        workshops.filter((c) => c.modified).forEach((c) => modified.push(c))

        const modifiedItems = modified.map((m) =>
            <ListGroup.Item
                action
                onClick={() => this.setState({viewDiff: m})}
                key={m.title}>
                {m.title}
            </ListGroup.Item>)

        return <div>
            <p>
                {modified.length === 1 ? "Eine Änderung" : (modified.length + " Änderungen")} am
                Programm:
            </p>
            <ListGroup>
                {modifiedItems}
            </ListGroup>
        </div>
    }

    private renderModificationHints(dataExport?: OfferData, pageExport?: Pages) {
        if (dataExport || pageExport) {
            return <div>
                <p>{this.renderModifiedProgramHint(dataExport)}</p>
                <p>{this.renderModifiedPagesHint(pageExport)}</p>
            </div>
        } else {
            return <p>Keine Änderungen im Programm- oder Texteditor vorgenommen</p>
        }
    }

    private static getDiffData(data: Course | Page): {title: string, content: string} {
        return {
            title: data.title,
            content: "content" in data ?
                data.content :
                stringify(Exporter.removeModificationFlag([data], true)[0], {space: 4})
        }
    }

    private renderDiffModal(): any {
        const diff = this.state.viewDiff;
        if (!diff) {
            return null
        }

        if (!this.props.import.data || !this.props.import.pages) {
            throw new Error("No import data available for diffing")
        }

        const candidates = [
            ...this.props.import.data.courses,
            ...this.props.import.data.workshops,
            ...this.props.import.data.kids,
            ...this.props.import.pages
        ].filter((c) => c.id === diff.id)

        const oldData = candidates[0]

        return <DiffModal
            onClose={() => this.setState({viewDiff: undefined})}
            old={Exporter.getDiffData(oldData)}
            new={Exporter.getDiffData(diff)}/>

    }



    render() {
        const {data, pages} = this.props.export;

        return <div>
            {this.renderModificationHints(data, pages)}
            {this.renderDiffModal()}
            <Button
                disabled={data == null && pages == null}
                onClick={this.onExport}>
                <div>
                    <div className={"mb-2"}>
                        <FiDownload size={"1.5em"}/>
                    </div>
                    <p>Export-Datei für Lieblingsadministrator herunterladen</p>
                </div>
            </Button>
        </div>
    }
}