import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import ReactDiffViewer, {DiffMethod} from 'react-diff-viewer';
import "./DiffModal.css"

interface DiffModalProps {
    new: { title: string, content: string }
    old: { title: string, content: string }
    onClose: () => void
}

export default class DiffModal extends React.Component<DiffModalProps> {
    constructor(props: DiffModalProps) {
        super(props);

        this.renderTitle = this.renderTitle.bind(this);
    }

    private renderTitle(): string {
        if (this.props.new.title === this.props.old.title) {
            return "Änderungen an " + this.props.new.title;
        }

        return `${this.props.old.title} => ${this.props.new.title}`
    }

    render() {
        return <Modal dialogClassName={"diffDialog"} show={true}
                      onHide={this.props.onClose}
                      size={"lg"}
                      backdrop="static">

            <Modal.Header closeButton>
                <Modal.Title>{this.renderTitle()}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <ReactDiffViewer
                    oldValue={this.props.old.content}
                    extraLinesSurroundingDiff={100}
                    compareMethod={DiffMethod.WORDS}
                    newValue={this.props.new.content}
                    splitView={true} />
            </Modal.Body>

            <Modal.Footer>
                <Button variant="primary" onClick={this.props.onClose}>OK</Button>
            </Modal.Footer>
        </Modal>
    }
}