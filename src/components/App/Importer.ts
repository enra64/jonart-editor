import JSZip from "jszip";
import {v4 as uuidv4} from "uuid";
import OfferData from "../../Data/OfferData";
import {Pages} from "../../Data/Pages";
import {Course} from "../../Data/Course";

async function extractData(zip: JSZip): Promise<OfferData> {
    const courses = await zip.file("data/courses.json")?.async("string")
    const kids = await zip.file("data/kinderkurse.json")?.async("string")
    const workshops = await zip.file("data/workshops.json")?.async("string")

    if (courses && kids && workshops) {
        return {
            courses: JSON.parse(courses).map((c: Course) => {
                return {...c, modified: false, id: uuidv4()}
            }),
            kids: JSON.parse(kids).map((c: Course) => {
                return {...c, modified: false, id: uuidv4()}
            }),
            workshops: JSON.parse(workshops).map((c: Course) => {
                return {...c, modified: false, id: uuidv4()}
            })
        }
    } else {
        throw new Error("content-archive is missing data files")
    }
}

async function extractPages(zip: JSZip): Promise<Pages> {
    const contentFolder = zip.folder("content");

    if (contentFolder) {
        const pagePromises: Promise<[string, string]>[] = [];
        contentFolder.forEach((path, file) => pagePromises.push(file.async("string").then((content) => [content, file.name])))

        let fileContents = await Promise.all(pagePromises);
        fileContents = fileContents.filter(([_, path]) => path.endsWith(".md"))

        return fileContents.map(([content, zipFileName]) => {
            const pathComponents = zipFileName.replace("content/", "").split("/")
            const category = pathComponents.splice(0, pathComponents.length - 1).join("/")
            const title = pathComponents[0]

            return {
                title,
                zipFileName,
                id: uuidv4(),
                category,
                modified: false,
                content
            }
        })
    } else {
        throw new Error("content-archive is missing content Folder")
    }
}

export async function extract(zip: JSZip): Promise<{ data: OfferData, pages: Pages }> {
    const data = await extractData(zip)
    const pages = await extractPages(zip)
    return {data, pages}
}

export async function fetchData(contentArchiveUrl: string): Promise<JSZip> {
    const response = await fetch(contentArchiveUrl);

    if (response.status === 200 || response.status === 0) {
        const zipBlob = await response.blob();
        return await JSZip.loadAsync(zipBlob);
    } else {
        throw new Error(response.statusText)
    }
}