import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import OfferEditorApp from "../Offers/OfferEditorApp/OfferEditorApp";
import React from "react";
import {Helmet} from 'react-helmet'
import {LinkContainer} from 'react-router-bootstrap'
import Exporter from "../Exporter/Exporter";
import Help from "../Help/Help";
import PageEditorApp from "../Pages/PageEditorApp/PageEditorApp";
import {Pages} from "../../Data/Pages";
import Button from "react-bootstrap/Button";
import OfferData from "../../Data/OfferData";
import {extract, fetchData} from "./Importer";

interface AppState {
    export: { data?: OfferData, pages?: Pages }
    import: { data?: OfferData, pages?: Pages }
    error?: string
}

const contentArchiveUrl = "https://www.jonart-malschule.de/content-archive.zip";

export default class App extends React.Component<{}, AppState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            import: {},
            export: {}
        }

        this.updateExportData = this.updateExportData.bind(this);
    }

    componentDidMount() {
        if (!this.state.import.data || !this.state.import.pages) {
            fetchData(contentArchiveUrl)
                .then((zip) => extract(zip)
                    .then((sourceData) => this.setState({import: sourceData})))
                .catch((e) => this.setState({error: "" + e}))
        }
    }

    private static renderNavbar(): any {
        return <div>
            <Navbar bg="dark" variant={"dark"} expand="lg">
                <LinkContainer to="/">
                    <Navbar.Brand href="#home">
                        <img
                            alt=""
                            src="/jonart-editor/favicon.png"
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                        />{' '}
                        JonArt-Dateneditor
                    </Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <LinkContainer to="/programmeditor">
                            <Nav.Link active={false}>Programmeditor</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/texteditor">
                            <Nav.Link active={false}>Texteditor</Nav.Link>
                        </LinkContainer>
                    </Nav>
                    <LinkContainer to="/exportieren">
                        <Button active={false} variant="outline-success">Exporteur</Button>
                    </LinkContainer>
                </Navbar.Collapse>
            </Navbar>
            <Helmet>
                <title>JonArt-Dateneditor</title>
            </Helmet>
        </div>
    }

    private static renderInContainer(content: any): any {
        return <Container>
            <div className={"py-4"}>
                {content}
            </div>
        </Container>
    }

    private updateExportData(offers?: OfferData, pages?: Pages) {
        this.setState((state) => {
            if (offers) {
                state.export.data = offers;
            }
            if (pages) {
                state.export.pages = pages;
            }
            return state
        })
    }

    render() {
        const data = this.state.export.data ? this.state.export.data : this.state.import.data;
        const pages = this.state.export.pages ? this.state.export.pages : this.state.import.pages;

        return <Router>
            {App.renderNavbar()}

            <Switch>
                <Route exact path="/">
                    <Help/>
                </Route>
                <Route path="/programmeditor">
                    {App.renderInContainer(
                        <OfferEditorApp
                            data={data}
                            onChange={(data) => this.updateExportData(data, undefined)}/>
                    )}
                </Route>
                <Route path={"/texteditor"}>
                    {App.renderInContainer(
                        <PageEditorApp
                            data={pages}
                            onChange={(pages) => this.updateExportData(undefined, pages)}/>)}
                </Route>
                <Route path={"/exportieren"}>
                    {App.renderInContainer(
                        <Exporter
                            import={this.state.import}
                            export={this.state.export}/>
                    )}
                </Route>
            </Switch>
        </Router>
    }
}