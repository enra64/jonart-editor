import Spinner from "react-bootstrap/Spinner";
import React from "react";
import "./LoadingSpinner.css"

export default class LoadingSpinner extends React.Component<{}, {}> {
    render() {
        return <div className={"loadingSpinner"}>
            <Spinner animation="border" role="status">
                <span className="sr-only">Lade...</span>
            </Spinner>
        </div>
    }
}